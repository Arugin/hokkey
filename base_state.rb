require './state_pattern'

class BaseState < StatePattern::State

  STRIKE_ANGLE = 1.0 * Math::PI / 180.0
  SWING_HEIGHT_COEF = [1.62, 3.15]
  MOVING_TO_DEF_POS_RANGE = 50
  DEF_POS_RANGE = 80
  STRIKE_OWN_PUCK_COEF = 70

  # Инициализируем переменные
  def init(me, world, game, move)
    strategy.me = me
    strategy.world = world
    strategy.game = game
    strategy.moving = move
    self.class.rink_width(strategy, game)
    self.class.rink_height(strategy, game)
    self.class.center_horizontal(strategy, game)
    self.class.center_vertical(strategy, game)
    init_units_speed
    init_units_acc
  end

  def init_units_speed
    strategy.prev_puck_speed = strategy.puck_speed || 0
    strategy.prev_units_speed = strategy.units_speed.dup
    active_hockeyists = strategy.world.hockeyists.select {|hockeyist| self.class.active_on_field?(hockeyist)}
    active_hockeyists.each do |hockeyist|
      strategy.units_speed[hockeyist.id] = self.class.unit_speed(hockeyist)
    end
    strategy.puck_speed = self.class.unit_speed(strategy.world.puck)
  end

  def init_units_acc
    strategy.units_speed.each_with_index do |unit, index|
      unless (unit.nil? || strategy.prev_units_speed[index].nil?)
        strategy.units_acc[index] = unit - strategy.prev_units_speed[index]
      end
    end
    strategy.puck_acc = strategy.puck_speed - strategy.prev_puck_speed
  end

  def puck_speed
    strategy.puck_speed
  end

  def puck_acc
    strategy.puck_acc
  end

  def hockeyist_speed(hockeyist)
    strategy.units_speed[hockeyist.id]
  end

  def hockeyist_acc(hockeyist)
    strategy.units_acc[hockeyist.id]
  end

  def move(me, world, game, move)
    strategy.init(me, world, game, move)
    strategy.detect_state
    strategy._move
  end

  # Only for substates
  def strategy
    stateful
  end

  # Определяем текущее состояние стратегии. Должно переопределяться в каждом наследнике состояний
  def detect_state

  end

  def detect_global_state
    strategy.transition_to(Object.const_get("#{who_owning_puck}OwningPuck"))
  end

  def who_owning_puck
    if nobody_own_puck?
      'Nobody'
    elsif player_own_puck?
      'Player'
    elsif opponent_own_puck?
      'Opponent'
    else
      raise "It's very strange, but we don't know, who owning puck!"
    end
  end

  # Переопределяем этот метод в классах-наследниках других состояний
  def _move
    raise 'This is abstract class, can\'t call _move here'
  end

  # Наш игрок владеет шайбой?
  def player_own_puck?
    strategy.world.puck.owner_player_id == strategy.me.player_id
  end

  def opponent_own_puck?
    not (nobody_own_puck? || player_own_puck?)
  end

  def nobody_own_puck?
    strategy.world.puck.owner_player_id == -1
  end

  # Наш хокеист владеет шайбой?
  def hockeyist_own_puck?(hockeyist = strategy.me)
    self.class.hockeyist_own_puck?(hockeyist, strategy.world)
  end

  # Наш хокеист владеет шайбой?
  def hockeyist_without_puck?(hockeyist = strategy.me)
     not hockeyist_own_puck?(hockeyist)
  end

  def strike_angle
    BaseState::STRIKE_ANGLE
  end

  def angle_to_net
    opponent_player = strategy.world.get_opponent_player

    net_x = 0.5 * (opponent_player.net_back + opponent_player.net_front)
    net_x += (opponent_net_on_left_rink? ? 0.33 : -0.33) * strategy.game.goal_net_width
    net_y = 0.5 * (opponent_player.net_bottom + opponent_player.net_top)
    net_y += (strategy.me.y < net_y ? 0.5 : -0.5) * strategy.game.goal_net_height

    strategy.me.get_angle_to(net_x, net_y)
  end

  def time_to_position(distance, unit)
    calc_time(distance, hockeyist_speed(unit), hockeyist_acc(unit))
  end

  def calc_time(distance, speed, acc)
    t = 100
    if !speed.nil? && speed > 0
      if !acc.nil? && acc.abs >= 0.5
        a = acc/2
        c = -distance
        d = speed * speed - 4 * a * c
        if d >= 0
          t = a > 0 ? (-speed + Math::sqrt(d))/(2*a) : (-speed - Math::sqrt(d))/(2*a)
        end
      else
        t = distance/speed
      end
      t = t > 100 ? 100 : t
    end
    t
  end

  def cancel_strike
    set_action(ActionType::CANCEL_STRIKE) if strategy.me.state == HockeyistState::SWINGING
  end

  def set_action(action)
    strategy.moving.action = action unless (strategy.me.state == HockeyistState::SWINGING && strategy.moving.action == ActionType::CANCEL_STRIKE)
    strategy.moving.action = ActionType::CANCEL_STRIKE if strategy.me.remaining_cooldown_ticks < 10 && strategy.me.last_action == ActionType::SWING && strategy.me.state == HockeyistState::SWINGING && !hockeyist_own_puck?
  end

  def hockeyist_on_top_rink?(hockeyist = strategy.me)
    hockeyist.y < strategy.center_vertical
  end

  def hockeyist_on_bottom_rink?
    not hockeyist_on_top_rink?
  end

  def opponent_net_on_left_rink?
    strategy.world.get_opponent_player.net_front < strategy.center_horizontal
  end

  def opponent_net_on_right_rink?
    not opponent_net_on_left_rink?
  end

  def swing_position
    delta_height = self.class.rand_range(*self.class::SWING_HEIGHT_COEF)
    delta_y = strategy.rink_height/delta_height
    y = hockeyist_on_top_rink? ? strategy.game.rink_top + delta_y : strategy.game.rink_bottom - delta_y
    x = self.class.rand_range(*x_range(y))
    {x: x, y: y}
  end

  def future_unit_position(unit, hockeyist = strategy.me)
    time_to_unit = time_to_position strategy.me.get_distance_to_unit(unit), hockeyist
    time_to_unit = time_to_unit > 30 ? 30 + (time_to_unit - 30)*0.2 :time_to_unit
    unit_position_through(unit, time_to_unit)
  end

  def future_unit_pass_position(unit, speed)
    time_to_unit = calc_time(strategy.me.get_distance_to_unit(unit), speed, -0.02)
    time_to_unit = time_to_unit > 30 ? 30 + (time_to_unit - 30)*0.1 :time_to_unit
    unit_position_through(unit, time_to_unit)
  end

  def two_opponents_near?
    hockeyists = strategy.world.hockeyists.select {|hockeyist| self.class.active_opponent?(hockeyist) }
    hockeyists.all? {|hockeyist| can_take_away_puck?(hockeyist)}
  end

  def can_take_away_puck?(hockeyist = strategy.me)
    puck = strategy.world.puck
    puck_near_stick?(hockeyist) && in_stick_sector?(hockeyist, puck)
  end

  def can_strike_unit?(unit)
    in_stick_sector?(strategy.me, unit) && unit_near_stick?(unit)
  end

  def unit_near_stick?(unit, hockeyist = strategy.me)
    hockeyist.get_distance_to_unit(unit) < strategy.game.stick_length
  end

  def puck_near_stick?(hockeyist = strategy.me)
    unit_near_stick?(strategy.world.puck, hockeyist)
  end

  def in_stick_sector?(me, target)
    me.get_angle_to_unit(target).abs < 0.5 * strategy.game.stick_sector
  end

  def unit_in_pass_sector?(target)
    in_pass_sector?(target.x, target.y)
  end

  def in_pass_sector?(x, y)
    strategy.me.get_angle_to(x, y).abs < 0.5 * strategy.game.pass_sector
  end

  def prepared_to_strike?
    in_good_strike_position? && angle_to_net.abs < strike_angle && can_take_away_puck?(strategy.me)
  end

  def in_good_strike_position?(hockeyist = strategy.me, me = true)
    hockeyist.x.between? *x_range(hockeyist.y, me)
  end

  def x_range(y, me = true)
    vertical = me_vertical_position
    position = opponent_net_on_left_rink?
    position = !position unless me
    horizontal = position ? 'left' : 'right'
    self.class.send("#{vertical}_#{horizontal}_strike_x_range", y)
  end

  def me_vertical_position
    hockeyist_on_top_rink? ? 'top' : 'bottom'
  end

  def in_danger_zone?(hockeyist = strategy.me)
    in_good_strike_position?(hockeyist, false)
  end
  #
  # Методы класса
  #

  def self.unit_speed(unit)
    Math.hypot(unit.speed_x, unit.speed_y)
  end

  def unit_position_through(unit, time)
    return [unit.x, unit.y] if unit.speed_x == 0.0 && unit.speed_y == 0.0

    acc = unit.respond_to?(:owner_hockeyist_id) ? puck_acc : hockeyist_acc(unit)
    position = Vector[unit.x, unit.y]
    direction = Vector[Math.cos(-unit.angle) * acc, Math.sin(unit.angle) * acc]
    velocity = Vector[unit.speed_x, unit.speed_y]

    optimal_direction = unit.respond_to?(:owner_hockeyist_id) ? velocity : velocity + direction

    x,y = *((position + optimal_direction * time).to_a)
    x = x < strategy.game.rink_left ? strategy.game.rink_left : x
    x = x > strategy.game.rink_right ? strategy.game.rink_right : x
    y = y < strategy.game.rink_top ? strategy.game.rink_top : y
    y = y > strategy.game.rink_bottom ? strategy.game.rink_bottom : y
    [x, y]
  end


  def speed_up_from_angle
    speed = hockeyist_speed(strategy.me) || 0
    if strategy.moving.turn.abs < Math::PI/4
      d = 1
    else
      if speed < strategy.game.hockeyist_speed_up_factor
        d = 0
      else
        d= 2 * self.class.speed_up_coef(strategy.moving.turn) - 1
      end
    end
    d
  end

  def defender_position
    my_player = strategy.world.get_my_player
    x = my_player.net_front
    x += my_player.net_front < strategy.center_horizontal ?  90 : -90
    y = 0.5 * (my_player.net_bottom + my_player.net_top)
    [x, y]
  end

  def me_closer_to_defender_position?(hockeyist = strategy.me)
    hockeyists = strategy.world.hockeyists.select {|_hockeyist| self.class.active_temmate?(_hockeyist)}
    hockeyist.id == self.class.nearest_hockeyist_to_position(*defender_position, hockeyists).id
  end

  def puck_owner
    strategy.world.hockeyists.select {|hockeyist| hockeyist.id == strategy.world.puck.owner_hockeyist_id}.first
  end

  def self.speed_up_coef(angle)
    (angle.abs - Math::PI/4)/(Math::PI - Math::PI/4)
  end

  def self.angle_between_vectors(v_a, v_b)
    Math.acos(v_a.normalize.inner_product(v_b.normalize))
  end

  def self.nearest_hockeyist(unit, hockeyists)
    nearest_hockeyist = nil
    nearest_hockeyist_range = 0.0

    hockeyists.each do |hockeyist|
      if block_given?
        next unless yield(hockeyist)
      end
      hockeyist_range = unit.get_distance_to_unit(hockeyist)
      if nearest_hockeyist.nil? || hockeyist_range < nearest_hockeyist_range
        nearest_hockeyist = hockeyist
        nearest_hockeyist_range = hockeyist_range
      end
    end
    nearest_hockeyist
  end

  def self.nearest_hockeyist_to_position(x, y, hockeyists)
    nearest_hockeyist = nil
    nearest_hockeyist_range = 0.0

    hockeyists.each do |hockeyist|
      if block_given?
        next unless yield(hockeyist)
      end
      hockeyist_range = hockeyist.get_distance_to(x, y)
      if nearest_hockeyist.nil? || hockeyist_range < nearest_hockeyist_range
        nearest_hockeyist = hockeyist
        nearest_hockeyist_range = hockeyist_range
      end
    end
    nearest_hockeyist
  end

  def self.rand_range(min, max)
    Random.rand * (max - min) + min
  end

  def self.top_left_strike_x_range(y)
    if y < 347
      min = 607.7 - 1.2*y         # 193, 347  340 224
      max = 680 - 0.77*y          # 412, 347  507,224
    else
      min = 450.45 - 0.74*y       # 193, 347  239,285
      max = 1608.23 - 3.44*y      # 281, 385  412,347
    end
    [min, max]
  end

  def self.bottom_left_strike_x_range(y)
    if y > 573
      min = 1.2*y - 491.8         # 193, 573  340, 696
      max = 0.77*y - 30.56        # 412, 573  507, 696
    else
      min = 0.74*y - 232          # 193, 573  239, 635
      max = 3.44*y - 1563         # 281, 535  412, 573
    end
    [min, max]
  end

  def self.top_right_strike_x_range(y)
    if y < 347
      min = 0.77*y + 520          #  788, 347  693,224
      max = 1.2*y + 592.3         # 1007, 347  860 224
    else
      min = 3.44*y - 408.23       #  919, 385  788,347
      max = 0.74*y + 749.53       # 1007, 347  961,285
    end
    [min, max]
  end

  def self.bottom_right_strike_x_range(y)
    if y > 573
      min = 1230 - 0.77*y         #  788, 573  693,696
      max = 1692 - 1.2*y          # 1007, 573  860 696
    else
      min = 2763 - 3.44*y         #  919, 535  788,573
      max = 1432 - 0.74 * y       # 1007, 573  961,635
    end
    [min, max]
  end

  def self.center_vertical(strategy, game)
    strategy.center_vertical ||= game.rink_top + self.rink_height(strategy, game)
  end

  def self.center_horizontal(strategy, game)
    strategy.center_horizontal ||= game.rink_left + self.rink_width(strategy, game)
  end

  def self.rink_width(strategy, game)
    strategy.rink_width ||= (game.rink_right - game.rink_left)/2
  end

  def self.rink_height(strategy, game)
    strategy.rink_height ||= (game.rink_bottom - game.rink_top)/2
  end

   # Ближайший оппонент
  def self.nearest_opponent(unit, world)
    self.nearest_hockeyist(unit, world.hockeyists) {|hockeyist| self.active_opponent?(hockeyist)}
  end

  def self.active_opponent?(hockeyist)
    not (self.teammate?(hockeyist) || self.goalte?(hockeyist) || self.hockeyist_unavailable?(hockeyist))
  end

  # Хоккеист у которого точно нет шайбы
  def self.hockeyist_unavailable?(hockeyist)
    hockeyist.state == HockeyistState::KNOCKED_DOWN || self.resting?(hockeyist)
  end

  def self.active_temmate?(hockeyist)
    not(self.goalte?(hockeyist)) && self.teammate?(hockeyist)
  end

  # Хокесит член нашей команды?
  def self.teammate?(hockeyist)
    hockeyist.teammate
  end

  def self.resting?(hockeyist)
    hockeyist.state == HockeyistState::RESTING
  end

  # хоккеист вратарь?
  def self.goalte?(hockeyist)
    hockeyist.type == HockeyistType::GOALIE
  end

  def self.active_on_field?(hockeyist)
    not(self.goalte?(hockeyist) || self.resting?(hockeyist))
  end

  def self.swining?(hockeyist)
    hockeyist.state == HockeyistState::SWINGING
  end

  def self.hockeyist_closer_to_puck(world, hockeyists = nil)
    hockeyists ||= world.hockeyists
    self.nearest_hockeyist(world.puck, hockeyists)
  end

  def self.me_closer_to_puck?(hockeyist, world)
    hockeyists = world.hockeyists.select {|_hockeyist| self.active_temmate?(_hockeyist)}
    hockeyist.id == self.hockeyist_closer_to_puck(world, hockeyists).id
  end

  def self.hockeyist_own_puck?(hockeyist, world)
    world.puck.owner_hockeyist_id == hockeyist.id
  end

  def self.hockeyist_without_puck?(hockeyist, world)
    not self.hockeyist_own_puck?(hockeyist, world)
  end

  def self.available_to_pass?(hockeyist, world)
    self.active_temmate?(hockeyist) && self.hockeyist_without_puck?(hockeyist, world) && !self.hockeyist_unavailable?(hockeyist) && hockeyist.remaining_cooldown_ticks == 0
  end

  def self.temmate_available_to_pass(world)
    hockeyists = world.hockeyists.select {|_hockeyist| self.available_to_pass?(_hockeyist, world) }
    hockeyists.first
  end

  def moving_to_goal_transition
    if strategy.me.get_distance_to(*defender_position) < self.class::MOVING_TO_DEF_POS_RANGE
      transition_to(WaitingPuckInGoal)
    elsif self.class.me_closer_to_puck?(strategy.me, strategy.world)
      transition_to(MovingToPuck)
    end
  end

  def waiting_puck_in_goal_transition
    if hockeyist_own_puck?
      transition_to(MovingToSwingPosition)
    elsif strategy.me.get_distance_to(*defender_position) > self.class::DEF_POS_RANGE
      transition_to(MovingToGoal)
    end
  end

end