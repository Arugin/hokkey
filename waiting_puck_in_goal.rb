class WaitingPuckInGoal < BaseAction

  def _move
    strategy.moving.turn = strategy.me.get_angle_to_unit(strategy.world.puck)
    defending_action
    strategy.moving.speed_up = 0
  end

  def enter
    strategy.class.anybody_in_def = true
  end

  def exit
    strategy.class.anybody_in_def = false
  end

end