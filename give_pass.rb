class GivePass < BaseAction

  def _move
    temmate_to_pass = self.class.temmate_available_to_pass(strategy.world)
    future_position = future_unit_pass_position(temmate_to_pass, 15)
    pass_angle = strategy.me.get_angle_to(*future_position)
    if in_pass_sector?(*future_position)
      strategy.moving.pass_power = 1.0
      strategy.moving.pass_angle = pass_angle
      set_action ActionType::PASS
    end
    strategy.moving.turn = pass_angle
    strategy.moving.speed_up = speed_up_from_angle
  end

end