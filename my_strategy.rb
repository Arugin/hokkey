include Math
require './matrix'
require './model/action_type'
require './model/game'
require './model/move'
require './model/hockeyist'
require './model/world'
require './state_pattern'

require './extensions'
# States
require './base_state'
require './base_action'
require './moving_to_puck'
require './moving_to_opponent_puck'
require './moving_to_swing_position'
require './blocking_opponent'
require './goal_strike'
require './goal_swing'
require './give_pass'
require './moving_to_goal'
require './waiting_puck_in_goal'
require './strike_owning_puck'

require './global_state'
require './player_owning_puck'
require './opponent_owning_puck'
require './nobody_owning_puck'



class MyStrategy
  # @param [Hockeyist] me
  # @param [World] world
  # @param [Game] game
  # @param [Move] move

  attr_accessor :me, :world, :game, :moving
  attr_accessor :rink_height, :rink_width, :center_vertical, :center_horizontal
  attr_accessor :units_speed, :units_acc, :prev_units_speed, :puck_speed, :puck_acc, :prev_puck_speed

  include StatePattern
  set_initial_state NobodyOwningPuck

  def initialize
    @units_speed = []
    @units_acc = []
    @prev_units_speed = []
    @prev_puck_speed = 0
    @@anybody_in_def = false
  end

  def self.anybody_in_def=(boolean)
    @@anybody_in_def = boolean
  end

  def self.anybody_in_def?
    @@anybody_in_def
  end

end