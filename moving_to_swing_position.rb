class MovingToSwingPosition < BaseAction

  def _move
    if @vertical_align != me_vertical_position
      take_position
    end
    if near_position?
      angle = angle_to_net
    else
      angle = strategy.me.get_angle_to(@swing_position[:x], @swing_position[:y])
    end
    strategy.moving.turn = angle
    strategy.moving.speed_up = speed_up_from_angle
  end

  def time_to_angle_to_net
    (angle_to_net/(strategy.game.hockeyist_turn_angle_factor)).abs
  end

  def time_to_position
    super distance_to_position, strategy.me
  end

  def on_position?
    near_position? && (angle_to_net.abs < strike_angle)
  end

  def distance_to_position
    strategy.me.get_distance_to(@swing_position[:x], @swing_position[:y])
  end

  def near_position?
    speed = hockeyist_speed(strategy.me) || 0
    @turn_to_net = (time_to_position <= time_to_angle_to_net + speed*speed*0.2 + strategy.game.swing_action_cooldown_ticks/2) if (!@turn_to_net && strategy.me.get_distance_to(@swing_position[:x], @swing_position[:y]) < 400)
    @turn_to_net
  end

  def take_position
    @swing_position = swing_position
    @vertical_align = me_vertical_position
  end

  def enter
    take_position
    @turn_to_net = false
  end

end