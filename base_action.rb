class BaseAction < BaseState

  def strategy
    stateful.stateful
  end

  # Определяем переключения для action
  def detect_state
    stateful.send("#{stateful.current_state_instance.class.name.underscore}_transition")
  end

  def defending_action
    if can_take_away_puck?
      if puck_speed <= 13
        set_action(ActionType::TAKE_PUCK)
      else
        set_action(ActionType::STRIKE)
      end
    elsif can_strike_unit?(self.class.nearest_opponent(strategy.me,strategy.world))
      set_action(ActionType::STRIKE)
    end
  end

end