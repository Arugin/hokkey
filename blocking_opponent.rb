class BlockingOpponent < BaseAction

  # Переопределяем этот метод в классах-наследниках других состояний
  def _move
    nearest_opponent = self.class.nearest_opponent(strategy.me, strategy.world)

    unless nearest_opponent.nil?
      strategy.moving.turn = strategy.me.get_angle_to(*future_unit_position(nearest_opponent))
      if !unit_near_stick?(nearest_opponent)
        strategy.moving.speed_up = speed_up_from_angle
      elsif in_stick_sector?(strategy.me, nearest_opponent)
        set_action(ActionType::STRIKE)
      end
    end
  end

end