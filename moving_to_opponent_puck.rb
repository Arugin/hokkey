class MovingToOpponentPuck < BaseAction

  def _move
    range_to_puck = 25
    opponent_with_puck = puck_owner
    position = Vector[*future_unit_position(opponent_with_puck)]
    direction = Vector[Math.cos(-opponent_with_puck.angle) * range_to_puck, Math.sin(opponent_with_puck.angle) * range_to_puck]
    strategy.moving.turn = strategy.me.get_angle_to(*((position + direction).to_a))
    strategy.moving.speed_up = speed_up_from_angle
    if opponent_with_puck.get_distance_to(*defender_position) > 550
      set_action(ActionType::TAKE_PUCK)
    elsif can_take_away_puck?
      set_action(ActionType::STRIKE)
    end

  end

end