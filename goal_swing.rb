class GoalSwing < BaseAction

  def _move
    set_action(ActionType::SWING) unless strategy.me.state == HockeyistState::SWINGING
  end

end