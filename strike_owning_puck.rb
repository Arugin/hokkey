class StrikeOwningPuck < BaseAction

  def _move
    strategy.moving.turn = strategy.me.get_angle_to_unit(strategy.world.puck)
    strategy.moving.speed_up = 1
    if can_take_away_puck?
      set_action(ActionType::STRIKE)
    end
  end

end