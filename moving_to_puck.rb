class MovingToPuck < BaseAction

  def _move
    strategy.moving.turn = strategy.me.get_angle_to(*future_unit_position(strategy.world.puck))
    strategy.moving.speed_up = speed_up_from_angle
    set_action(ActionType::TAKE_PUCK)
  end

end