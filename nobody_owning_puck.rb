class NobodyOwningPuck < GlobalState

  set_initial_state MovingToPuck

  def detect_state
    if nobody_own_puck?
      super
    else
      strategy.detect_global_state
    end
  end

  def blocking_opponent_transition
    if prepared_to_strike?
      transition_to(GoalStrike)
    elsif two_opponents_near? && can_take_away_puck?
      transition_to(GoalStrike)
    elsif self.class.me_closer_to_puck?(strategy.me, strategy.world)
      transition_to(MovingToPuck)
    end
  end

  def moving_to_puck_transition
    if prepared_to_strike?
      transition_to(GoalStrike)
    elsif two_opponents_near? && can_take_away_puck?
      transition_to(GoalStrike)
    elsif !self.class.me_closer_to_puck?(strategy.me, strategy.world) && !strategy.class.anybody_in_def?
      transition_to(MovingToGoal)
    end
  end

  def goal_strike_transition
    unless (prepared_to_strike? || (two_opponents_near? && can_take_away_puck?))
      transition_to(MovingToPuck)
    end
  end

  def waiting_puck_in_goal_transition
    if hockeyist_own_puck?
      transition_to(MovingToSwingPosition)
    elsif strategy.me.get_distance_to_unit(strategy.world.puck) < 300 && strategy.me.get_distance_to_unit(self.class.nearest_opponent(strategy.me, strategy.world)) > 500
      transition_to(MovingToPuck)
    elsif strategy.me.get_distance_to(*defender_position) > self.class::DEF_POS_RANGE
      transition_to(MovingToGoal)
    end
  end

  def enter
    strategy.class.anybody_in_def = false
    return if strategy.me.nil?
    if !self.class.me_closer_to_puck?(strategy.me, strategy.world) && !strategy.class.anybody_in_def?
      transition_to(MovingToGoal)
    end
  end

end