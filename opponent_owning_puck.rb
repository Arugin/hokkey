class OpponentOwningPuck < GlobalState

  set_initial_state MovingToOpponentPuck

  def detect_state
    if opponent_own_puck?
      super
    else
      strategy.detect_global_state
    end
  end

  def blocking_opponent_transition
    if in_danger_zone? && can_take_away_puck?
      transition_to(GoalStrike)
    elsif prepared_to_strike?
      transition_to(GoalStrike)
    elsif self.class.me_closer_to_puck?(strategy.me, strategy.world)
      transition_to(MovingToOpponentPuck)
    end
  end

  def moving_to_opponent_puck_transition
    if in_danger_zone? && can_take_away_puck?
      transition_to(GoalStrike)
    elsif prepared_to_strike?
      transition_to(GoalStrike)
    elsif me_closer_to_defender_position? && !strategy.class.anybody_in_def?
      transition_to(MovingToGoal)
    end
  end

  def goal_strike_transition
    unless (prepared_to_strike? || in_danger_zone?)
      transition_to(MovingToOpponentPuck)
    end
  end

  def waiting_puck_in_goal_transition
    if hockeyist_own_puck?
      transition_to(MovingToSwingPosition)
    elsif strategy.me.get_distance_to(*defender_position) > self.class::DEF_POS_RANGE && strategy.me.get_distance_to_unit(puck_owner) > 500
      transition_to(MovingToGoal)
    elsif strategy.me.get_distance_to_unit(puck_owner) < self.class::STRIKE_OWN_PUCK_COEF * hockeyist_speed(puck_owner)  && puck_owner.state != HockeyistState::SWINGING
      transition_to(StrikeOwningPuck)
    end
  end

  def moving_to_goal_transition
    if strategy.me.get_distance_to(*defender_position) < self.class::MOVING_TO_DEF_POS_RANGE
      transition_to(WaitingPuckInGoal)
    elsif self.class.me_closer_to_puck?(strategy.me, strategy.world)
      transition_to(MovingToOpponentPuck)
    end
  end

  def strike_owning_puck_transition
    if !strategy.class.anybody_in_def? && puck_owner.get_distance_to(*defender_position) > self.class::STRIKE_OWN_PUCK_COEF * 5
      transition_to(MovingToGoal)
    end
  end

  def enter
    strategy.class.anybody_in_def = false
    if !self.class.me_closer_to_puck?(strategy.me, strategy.world) && !strategy.class.anybody_in_def?
      transition_to(MovingToGoal)
    end
  end

end