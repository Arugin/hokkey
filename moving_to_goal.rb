class MovingToGoal < BaseAction

  def _move
    distance = strategy.me.get_distance_to(*@defender_position)
    if distance < self.class::MOVING_TO_DEF_POS_RANGE
      strategy.moving.speed_up = 0
      transition_to(WaitingPuckInGoal)
    end
    strategy.moving.turn = strategy.me.get_angle_to(*@defender_position)
    if distance < stop_range
      speed_up = stop_moving(distance)
    else
      speed_up = speed_up_from_angle
    end
    defending_action
    strategy.moving.speed_up = speed_up
  end

  def opponent_net_center
    opponent_player = strategy.world.get_opponent_player

    x = opponent_player.net_front
    y = 0.5 * (opponent_player.net_bottom + opponent_player.net_top)
    [x, y]
  end

  def goalte
    strategy.world.hockeyists.find {|hockeyist| self.class.goalte?(hockeyist)}
  end

  def stop_range
    hockeyist_speed(strategy.me) > 5 ? 250 :120
  end

  def stop_moving(distance)
    speed_up = 1
    me = strategy.me
    return 1 if hockeyist_acc(me).nil?
    if hockeyist_acc(me).abs > 0.02
      if time_to_position(distance, me) - hockeyist_speed(me).abs/hockeyist_acc(me).abs <= 1
        if hockeyist_acc(me).abs < strategy.game.hockeyist_speed_down_factor
          speed_up = -hockeyist_acc(me)/strategy.game.hockeyist_speed_down_factor
        else
          speed_up = -1
        end
      else
        if time_to_position(distance, me) > 35
          speed_up = 1
        else
          speed_up = 0.5
        end
      end
    else
      if time_to_position(distance, me) > 35
        speed_up = 0.4
      elsif time_to_position(distance, me) > 5
        speed_up = 0.1
      else
        speed_up = 0
      end
    end
    speed_up
  end

  def enter
    @defender_position ||= defender_position
    strategy.class.anybody_in_def = true
  end

  def exit
    strategy.class.anybody_in_def = false
  end

end