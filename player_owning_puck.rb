class PlayerOwningPuck < GlobalState

  set_initial_state MovingToSwingPosition
  OWNING_COOLDOWN = 150

  def detect_state
    if player_own_puck?
      super
    else
      strategy.detect_global_state
    end
  end

  def moving_to_swing_position_transition
    if !(hockeyist_own_puck? || two_opponents_near?) && !strategy.class.anybody_in_def?
      transition_to(MovingToGoal)
    elsif on_position?
      transition_to(GoalSwing)
    elsif two_opponents_near? && hockeyist_own_puck? && (strategy.world.tick - @enter_tick > self.class::OWNING_COOLDOWN)
      self.class.temmate_available_to_pass(strategy.world).nil? ? transition_to(GoalStrike) : transition_to(GivePass)
    end
  end

  def goal_swing_transition
    transition_to(GoalStrike)
  end

  def goal_strike_transition

  end

  def give_pass_transition
    if two_opponents_near?
      transition_to(GoalStrike) if self.class.temmate_available_to_pass(strategy.world).nil?
    else
      transition_to(MovingToSwingPosition)
    end
  end

  def blocking_opponent_transition
    if two_opponents_near? && !can_take_away_puck?
      transition_to(MovingToSwingPosition)
    end
  end

  def moving_to_goal_transition
    if strategy.me.get_distance_to(*defender_position) < self.class::DEF_POS_RANGE
      transition_to(WaitingPuckInGoal)
    end
  end

  def enter
    @enter_tick = strategy.world.tick
    strategy.class.anybody_in_def = false
    if !self.class.me_closer_to_puck?(strategy.me, strategy.world) && !strategy.class.anybody_in_def?
      transition_to(MovingToGoal)
    end
  end

  def exit
    cancel_strike
  end

end